# ELECTRONIC VOTING #

This system allow to citizens avoid, during political elections, cut the paper and enclose it into envelope. Also it avoids manual identification of persons, allowing safer control over who has already voted and who not.

![03.jpg](https://bitbucket.org/repo/rA7kL4/images/566884503-03.jpg)

### Details of this project ###
* This project was developed on 2014, during the Object Oriented II university subject.
* Developed on **JAVA** language under **MVC** architectural pattern.
* Integrated with **MySQL** database via **Hibernate** Java Framework.
* Implemented Java library that's established the connection between the web camera and the system.
* The other Java library that allows read and translate the **PDF417** code (similar to QR code).

### Steps to follow to voting ###
1. The voter ID card is passed through the reader **PDF417** codes.
2. On the screen shows the data of the citizen, while the second screen (located behind the curtains) voting option is enabled.
3. The voter selects candidates (separately). And confirms the election.
4. Later in the screen of the table authorities it figures that the vote was already done.
5. Finally, is printed a ticket that serves as proof of the voting.

![DER.png](https://bitbucket.org/repo/rA7kL4/images/4043958854-DER.png)
package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.Administracion;
import model.AltaAutoridadDeMesa;
import model.AltaCandidato;
import model.AltaFiscalDeMesa;
import model.AltaPartido;
import view.AdministracionGUI;
import view.altas.AltaAutoridadDeMesaGUI;
import view.altas.AltaCandidatoGUI;
import view.altas.AltaFiscalDeMesaGUI;
import view.altas.AltaPartidoGUI;

public class AdministracionCtrl {

	private AdministracionGUI theView;
	private Administracion theModel;

	public AdministracionCtrl(AdministracionGUI theView, Administracion theModel) {
		this.theView = theView;
		this.theModel = theModel;
		this.theView
				.altaAutoridadDeMesaListener(new AltaAutoridadDeMesaListener());
		this.theView.altaFiscalDeMesaListener(new AltaFiscalDeMesaListener());
		this.theView.altaCandidatoListener(new AltaCandidatoListener());
		this.theView.asignarMesasListener(new AsignarMesasListener());
		this.theView.finalizarVotacionListener(new FinalizarVotacionListener());
		this.theView.altaPartidoListener(new AltaPartidoListener());
	}

	private void volverAlMenu() {
		AdministracionGUI administracionGUI = new AdministracionGUI();
		new AdministracionCtrl(administracionGUI, theModel);
		theView.setVisible(false);
		administracionGUI.setVisible(true);
	}

	class AltaPersonaListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			AdministracionGUI administracionGUI = new AdministracionGUI();
			new AdministracionCtrl(administracionGUI, theModel);
			theView.setVisible(false);
			administracionGUI.setVisible(true);
		}
	}

	class AltaPartidoListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			AltaPartidoGUI altaPartidoGUI = new AltaPartidoGUI();
			new AltaPartidoCtrl(altaPartidoGUI, new AltaPartido());
			theView.setVisible(false);
			altaPartidoGUI.setVisible(true);
		}
	}

	class AltaInstitucionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			AdministracionGUI administracionGUI = new AdministracionGUI();
			new AdministracionCtrl(administracionGUI, theModel);
			theView.setVisible(false);
			administracionGUI.setVisible(true);
		}
	}

	class AsignarMesasListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				theModel.altaDeMesas();
				theModel.asignarVotantes();
				theView.displayInfoMessage("Asignaci�n de padr�n exitosa!");
			} catch (Exception e1) {
				theView.displayErrorMessage(e1.getMessage());
			}

		}
	}

	class AltaAutoridadDeMesaListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			AltaAutoridadDeMesaGUI altaAutoridadDeMesaGUI = new AltaAutoridadDeMesaGUI();
			new AltaAutoridadDeMesaCtrl(altaAutoridadDeMesaGUI,
					new AltaAutoridadDeMesa());
			theView.setVisible(false);
			altaAutoridadDeMesaGUI.setVisible(true);
		}
	}

	class AltaFiscalDeMesaListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			AltaFiscalDeMesaGUI altaFiscalDeMesaGUI = new AltaFiscalDeMesaGUI();
			new AltaFiscalDeMesaCtrl(altaFiscalDeMesaGUI,
					new AltaFiscalDeMesa());
			theView.setVisible(false);
			altaFiscalDeMesaGUI.setVisible(true);
		}
	}

	class AltaCandidatoListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			AltaCandidatoGUI altaCandidatoGUI = new AltaCandidatoGUI();
			new AltaCandidatoCtrl(altaCandidatoGUI, new AltaCandidato());
			theView.setVisible(false);
			altaCandidatoGUI.setVisible(true);
		}
	}

	class IniciarVotacionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			AdministracionGUI administracionGUI = new AdministracionGUI();
			new AdministracionCtrl(administracionGUI, theModel);
			theView.setVisible(false);
			administracionGUI.setVisible(true);
		}
	}

	class FinalizarVotacionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			theView.displayInfoMessage("Se eliminaron "
					+ theModel.finalizarVotacion() + " mesas.");
		}
	}
}

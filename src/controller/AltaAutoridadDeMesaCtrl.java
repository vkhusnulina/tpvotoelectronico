package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import model.Administracion;
import model.AltaAutoridadDeMesa;
import model.entidades.Mesa;
import model.entidades.Persona;
import view.AdministracionGUI;
import view.altas.AltaAutoridadDeMesaGUI;

public class AltaAutoridadDeMesaCtrl {

	private AltaAutoridadDeMesaGUI theView;
	private AltaAutoridadDeMesa theModel;

	public AltaAutoridadDeMesaCtrl(AltaAutoridadDeMesaGUI theView,
			AltaAutoridadDeMesa theModel) {
		this.theView = theView;
		this.theModel = theModel;
		List<Mesa> lst = new Mesa().searchAll();
		this.theView.setMesas(lst);
		this.theView.buscarListener(new BuscarListener());
		this.theView.crearListener(new CrearListener());
	}

	class BuscarListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String dni;
			try {
				dni = theView.getDni();
				Persona persona = theModel.buscarPersonaPorDni(Long
						.parseLong(dni));
				theView.setNombre(persona.getNombre());
				theView.setApellido(persona.getApellido());
				theView.setSexo(persona.getSexo());
				theView.setNacionalidad(persona.getNacionalidad());
			} catch (Exception e1) {
				theView.displayErrorMessage(e1.getMessage());
			}
		}
	}

	class CrearListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				// theView.displayInfoMessage(theModel.altaAutoridadDeMesa(theView
				// .getMesa()));
				theView.displayInfoMessage(theModel.altaAutoridadDeMesa(theView
						.getMesa()));
				AdministracionGUI administracionGUI = new AdministracionGUI();
				new AdministracionCtrl(administracionGUI, new Administracion());
				theView.setVisible(false);
				administracionGUI.setVisible(true);
			} catch (Exception e1) {
				theView.displayErrorMessage(e1.getMessage());
			}

		}
	}

}
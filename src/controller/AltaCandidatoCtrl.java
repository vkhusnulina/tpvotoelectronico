package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.Administracion;
import model.AltaCandidato;
import model.entidades.Persona;
import view.AdministracionGUI;
import view.altas.AltaCandidatoGUI;

public class AltaCandidatoCtrl {

	private AltaCandidatoGUI theView;
	private AltaCandidato theModel;

	public AltaCandidatoCtrl(AltaCandidatoGUI theView, AltaCandidato theModel) {
		this.theView = theView;
		this.theModel = theModel;
		this.theView.setCargos(theModel.getLstCargos());
		this.theView.setPartidos(theModel.getLstPartidos());
		this.theView.buscarListener(new BuscarListener());
		this.theView.crearListener(new CrearListener());
	}

	class BuscarListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String dni;
			try {
				dni = theView.getDni();
				Persona persona = theModel.buscarPersonaCandidataPorDni(Long
						.parseLong(dni));
				theView.setNombre(persona.getNombre());
				theView.setApellido(persona.getApellido());
				theView.setSexo(persona.getSexo());
				theView.setNacionalidad(persona.getNacionalidad());
			} catch (Exception e1) {
				theView.displayErrorMessage(e1.getMessage());
			}
		}
	}

	class CrearListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				theView.displayInfoMessage(theModel.altaCandidato(
						theView.getCargo(), theView.getPartido(), "NULL"));
				AdministracionGUI administracionGUI = new AdministracionGUI();
				new AdministracionCtrl(administracionGUI, new Administracion());
				theView.setVisible(false);
				administracionGUI.setVisible(true);
			} catch (Exception e1) {
				theView.displayErrorMessage(e1.getMessage());
			}

		}
	}

}

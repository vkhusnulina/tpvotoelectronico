package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.Administracion;
import model.AltaFiscalDeMesa;
import model.entidades.Persona;
import view.AdministracionGUI;
import view.altas.AltaFiscalDeMesaGUI;

public class AltaFiscalDeMesaCtrl {

	private AltaFiscalDeMesaGUI theView;
	private AltaFiscalDeMesa theModel;

	public AltaFiscalDeMesaCtrl(AltaFiscalDeMesaGUI theView,
			AltaFiscalDeMesa theModel) {
		this.theView = theView;
		this.theModel = theModel;
		this.theView.setMesas(theModel.getLstMesas());
		this.theView.setPartidos(theModel.getLstPartidos());
		this.theView.buscarListener(new BuscarListener());
		this.theView.crearListener(new CrearListener());
	}

	class BuscarListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String dni;
			try {
				dni = theView.getDni();
				Persona persona = theModel.buscarPersonaPorDni(Long
						.parseLong(dni));
				theView.setNombre(persona.getNombre());
				theView.setApellido(persona.getApellido());
				theView.setSexo(persona.getSexo());
				theView.setNacionalidad(persona.getNacionalidad());
			} catch (Exception e1) {
				theView.displayErrorMessage(e1.getMessage());
			}
		}
	}

	class CrearListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				theView.displayInfoMessage(theModel.altaFiscalDeMesa(
						theView.getPartido(), theView.getMesa()));
				AdministracionGUI administracionGUI = new AdministracionGUI();
				new AdministracionCtrl(administracionGUI, new Administracion());
				theView.setVisible(false);
				administracionGUI.setVisible(true);
			} catch (Exception e1) {
				theView.displayErrorMessage(e1.getMessage());
			}

		}
	}

}

package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.Administracion;
import model.AltaPartido;
import view.AdministracionGUI;
import view.altas.AltaPartidoGUI;

public class AltaPartidoCtrl {

	private AltaPartidoGUI theView;
	private AltaPartido theModel;

	public AltaPartidoCtrl(AltaPartidoGUI theView, AltaPartido theModel) {
		this.theView = theView;
		this.theModel = theModel;
		this.theView.addGuardarListener(new CrearListener());
	}

	class CrearListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				theModel.agregar(theView.getNombre(), theView.getImagen(),
						theView.getCPrimario(), theView.getCSecundario());
				theView.displayInfoMessage("Creaci�n exitosa del partido "
						+ theView.getNombre());

				AdministracionGUI administracionGUI = new AdministracionGUI();
				new AdministracionCtrl(administracionGUI, new Administracion());
				theView.setVisible(false);
				administracionGUI.setVisible(true);
			} catch (Exception e1) {
				theView.displayErrorMessage(e1.getMessage());
			}

		}
	}

}

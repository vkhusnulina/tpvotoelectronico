package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.Administracion;
import model.AutentificacionDelVotante;
import model.StringUtil;
import model.entidades.Mesa;
import model.entidades.Voto;
import view.AdministracionGUI;
import view.votacion.AutentificacionDelVotanteGUI;

public class AutentificacionDelVotanteCtrl {
	private AutentificacionDelVotanteGUI view;
	private AutentificacionDelVotante model;
	private Mesa mesa;
	private int votantes = 0;

	public AutentificacionDelVotanteCtrl(AutentificacionDelVotanteGUI view,
			AutentificacionDelVotante model, Mesa mesa) {
		votantes = model.cantidadDeVotantes(mesa);
		this.view = view;
		this.model = model;
		this.mesa = mesa;
		this.view.setVisible(true);
		this.view.addBuscarListener(new BuscarListener());
		this.view.addIniciarListener(new IniciarListener());
		this.view.addImprimirListener(new ImprimirListener());
		this.view.setPersonasRestantes(String.valueOf(votantes));
	}

	class BuscarListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String dni;
			try {
				dni = view.getDni();
				Voto voto = model.buscarVoto(Long.parseLong(dni), mesa);
				view.setNombreC(voto.getPersona().toString());
				view.setEdad(String.valueOf(voto.getPersona().getEdad())
						+ " a�os.");
				view.setSexo(voto.getPersona().getSexo().toUpperCase());
				view.setNacionalidad(voto.getPersona().getNacionalidad()
						.toUpperCase());
				view.setPaisOrigen(voto.getPersona().getPais_origen()
						.toUpperCase());
				view.setDireccion(voto.getPersona().getDireccion().toString());
				view.setFNacimiento(StringUtil.GregorianCalendarToString(voto
						.getPersona().getFecha_nacimiento()));
				view.setVoto(voto.getVotoRealizado());
				if (voto.getVoto_realizado() == 0)
					view.enableIniciar(true);
				else
					view.displayWarningMessage("La persona ya vot�.");
			} catch (Exception e1) {
				view.enableIniciar(false);
				view.displayErrorMessage(e1.getMessage());
			}
		}
	}

	class IniciarListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				votantes -= 1;
				AdministracionGUI administracionGUI = new AdministracionGUI();
				new AdministracionCtrl(administracionGUI, new Administracion());
				view.setEnabled(false);
				administracionGUI.setVisible(true);
			} catch (Exception e1) {
				view.displayErrorMessage(e1.getMessage());
			}

		}
	}

	class ImprimirListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				AdministracionGUI administracionGUI = new AdministracionGUI();
				new AdministracionCtrl(administracionGUI, new Administracion());
				view.setVisible(false);
				administracionGUI.setVisible(true);
			} catch (Exception e1) {
				view.displayErrorMessage(e1.getMessage());
			}

		}
	}

}

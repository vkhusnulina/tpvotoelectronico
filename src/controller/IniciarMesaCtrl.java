package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.AutentificacionDelVotante;
import model.IniciarMesa;
import model.entidades.AutoridadDeMesa;
import model.entidades.Fiscal;
import model.entidades.Mesa;
import view.votacion.AutentificacionDelVotanteGUI;
import view.votacion.IniciarMesaGUI;

public class IniciarMesaCtrl {

	private IniciarMesaGUI view;
	private IniciarMesa model;
	private Mesa mesa;

	public IniciarMesaCtrl(IniciarMesaGUI view, IniciarMesa model) {
		this.view = view;
		this.model = model;
		this.view.setVisible(true);
		this.view.addBuscarListener(new BuscarListener());
		this.view.addIniciarListener(new IniciarListener());
	}

	class BuscarListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String dni;
			String agentes = "";
			try {
				dni = view.getDni();
				AutoridadDeMesa autoridad = model.buscarAutoridadDeMesa(Long
						.parseLong(dni));
				if (autoridad != null) {
					mesa = autoridad.getMesa();
					view.setMesa(mesa.getNumero());
					view.setInstitucion(mesa.getInstitucion().getNombre());
					agentes += "Autoridades de Mesa:\n";
					for (AutoridadDeMesa a : model.traerAutoridadesDeMesa(mesa)) {
						agentes += a.getPersona().toString() + "\n";
					}
					agentes += "\nFiscales de Mesa:\n";
					for (Fiscal f : model.traerFiscalesDeMesa(mesa)) {
						agentes += f.getPersona().toString() + " (Lista "
								+ f.getPartido().getNombre() + ").\n";
					}
					view.setIntegrantes(agentes);
					view.enableIniciar(true);
				} else {
					view.enableIniciar(false);
					view.displayWarningMessage("La persona con el DNI "
							+ dni
							+ " no se encuentra registrada como Autoridad de Mesa!");
				}
			} catch (Exception e1) {
				view.enableIniciar(false);
				view.displayErrorMessage(e1.getMessage());
			}
		}
	}

	class IniciarListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				new AutentificacionDelVotanteCtrl(
						new AutentificacionDelVotanteGUI(),
						new AutentificacionDelVotante(), mesa);
				view.setVisible(false);
			} catch (Exception e1) {
				view.displayErrorMessage(e1.getMessage());
			}

		}
	}
}

package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.Administracion;
import view.AdministracionGUI;
import view.LogInGUI;

public class LogInCtrl {

	private LogInGUI theView;
	private Administracion theModel;

	public LogInCtrl(LogInGUI theView, Administracion theModel) {
		this.theView = theView;
		this.theModel = theModel;

		this.theView.addLogInListener(new LogInListener());
	}

	class LogInListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String user, pass;
			try {
				user = theView.getUser();
				pass = theView.getPassword();
				if (theModel.logIn(user, pass)) {
					AdministracionGUI administracionGUI = new AdministracionGUI();
					new AdministracionCtrl(administracionGUI, theModel);
					theView.setVisible(false);
					administracionGUI.setVisible(true);
				} else
					theView.displayErrorMessage("El usuario ingresado no existe");
			} catch (Exception e1) {
				theView.displayErrorMessage(e1.getMessage());
			}
		}
	}

}

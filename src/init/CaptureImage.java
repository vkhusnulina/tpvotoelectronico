package init;

import static org.bytedeco.javacpp.opencv_highgui.cvSaveImage;

import org.bytedeco.javacpp.opencv_core.IplImage;
import org.bytedeco.javacv.FrameGrabber.Exception;
import org.bytedeco.javacv.OpenCVFrameGrabber;

public class CaptureImage {
	private static void captureFrame() {
		// 0-default camera, 1 - next...so on
		final OpenCVFrameGrabber grabber = new OpenCVFrameGrabber(0);
		sacarFoto(grabber);
	}

	public static void main(String[] args) {
		captureFrame();
	}

	public static void sacarFoto(OpenCVFrameGrabber grabber) {
		try {
			grabber.setImageWidth(1280);
			grabber.setImageHeight(720);
			grabber.setBitsPerPixel(24);
			grabber.start();
			IplImage img = grabber.grab();
			if (img != null) {
				cvSaveImage("capture.jpg", img);
				System.out.println("ssd");
				// Reader lector = new MultiFormatReader();

				// LuminanceSource fuente = new BufferedImageLuminanceSource(
				// img.getBufferedImage());
				// BinaryBitmap mapaBits = new BinaryBitmap(new HybridBinarizer(
				// fuente));
				// Result resultado = lector.decode(mapaBits);
				// System.out.println("Contenido del codigo = "
				// + resultado.getText());

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// } catch (NotFoundException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// sacarFoto(grabber);
			// } catch (ChecksumException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// } catch (FormatException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// } finally {

		}
	}
}
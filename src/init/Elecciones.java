package init;

import model.IniciarMesa;
import view.votacion.IniciarMesaGUI;
import controller.IniciarMesaCtrl;

public class Elecciones {

	public Elecciones() {
	}

	public static void main(String[] args) {
		try {
			new IniciarMesaCtrl(new IniciarMesaGUI(), new IniciarMesa());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}

package init;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.Reader;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;

public class Test {

	public Test() {

		Reader lector = new MultiFormatReader();
		File ubicacionImagen = new File(
				"C:/Users/Valeria/git/Local/New folder/src/04.jpg");
		BufferedImage imagen;

		if (ubicacionImagen.exists()) {
			try {
				imagen = ImageIO.read(ubicacionImagen);
				LuminanceSource fuente = new BufferedImageLuminanceSource(
						imagen);
				BinaryBitmap mapaBits = new BinaryBitmap(new HybridBinarizer(
						fuente));
				Result resultado = lector.decode(mapaBits);
				System.out.println("Contenido del codigo = "
						+ resultado.getText());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ChecksumException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		new Test();
	}
}

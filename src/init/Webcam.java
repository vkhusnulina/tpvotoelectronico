package init;

import org.bytedeco.javacpp.opencv_core.IplImage;
import org.bytedeco.javacpp.opencv_highgui;
import org.bytedeco.javacpp.opencv_highgui.CvCapture;
import org.bytedeco.javacv.CanvasFrame;

public class Webcam {

	public static void main(String[] args) throws Exception {
		CvCapture capture = opencv_highgui.cvCreateCameraCapture(0);

		opencv_highgui.cvSetCaptureProperty(capture,
				opencv_highgui.CV_CAP_PROP_FRAME_HEIGHT, 720);
		opencv_highgui.cvSetCaptureProperty(capture,
				opencv_highgui.CV_CAP_PROP_FRAME_WIDTH, 1280);

		IplImage grabbedImage = opencv_highgui.cvQueryFrame(capture);

		CanvasFrame frame = new CanvasFrame("Webcam");

		while (frame.isVisible()
				&& (grabbedImage = opencv_highgui.cvQueryFrame(capture)) != null) {
			frame.showImage(grabbedImage);
		}

		frame.dispose();
		opencv_highgui.cvReleaseCapture(capture);
	}

}
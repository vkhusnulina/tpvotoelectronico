package model;

import java.util.List;

import model.entidades.Administrador;
import model.entidades.Direccion;
import model.entidades.Institucion;
import model.entidades.Localidad;
import model.entidades.Mesa;
import model.entidades.Persona;
import model.entidades.Voto;

public class Administracion {

	public Administracion() {
	}

	public boolean logIn(String user, String password) throws Exception {
		if (user.isEmpty() || password.isEmpty())
			throw new Exception("Falta completar campos requeridos");
		Administrador admin = new Administrador();
		admin.setUsername(user);
		admin.setPassword(password);
		if (admin.searchUnique() != null)
			return true;
		else
			return false;
	}

	public void altaDeMesas() throws Exception {
		List<Institucion> lstInst = new Institucion().searchAll();
		int num = 0;
		for (Institucion inst : lstInst) {
			for (int i = 1; i <= 10; i++) {
				Mesa mesa = new Mesa();
				mesa.setInstitucion(inst);
				mesa.setNumero(num + i);
				mesa.create();
			}
			num += 10;
		}
		System.out.println("alta de mesas OK");
	}

	private int grupoDeMesa(StringUtil.ABC letter) {
		switch (letter) {
		case A:
		case B:
			return 1;
		case C:
		case D:
			return 2;
		case E:
		case F:
			return 3;
		case G:
		case H:
		case I:
			return 4;
		case J:
		case K:
		case L:
			return 5;
		case M:
		case N:
		case �:
			return 6;
		case O:
		case P:
		case Q:
			return 7;
		case R:
		case S:
			return 8;
		case T:
		case U:
			return 9;
		case V:
		case W:
		case X:
		case Y:
		case Z:
			return 10;
		default:
			return 0;
		}
	}

	public void asignarVotantes() throws Exception {
		List<Localidad> lstLocl = new Localidad().searchAll();
		for (Localidad lcl : lstLocl) {
			Direccion dr = new Direccion();
			dr.setLocalidad(lcl);
			dr = dr.searchUnique();
			// Obtiene la institucion de la localidad->direccion
			Institucion inst = new Institucion();
			inst.setDireccion(dr);
			inst = inst.searchUnique();
			// Obtiene mesas asignadas a la institucion
			Mesa msa = new Mesa();
			msa.setInstitucion(inst);
			List<Mesa> lstMsa = msa.searchAll();
			// Obtiene personas recididas en la localidad
			List<Persona> lstPrsn = new Persona()
					.buscarPersonasPorLocalidadId(lcl.getId());
			// Realiza asignaciones de mesas a personas
			for (Persona p : lstPrsn) {
				if (p.getIdNacionalidad() != 4 && p.getEdad() >= 18) {
					int grupo = grupoDeMesa(StringUtil.getFirstLetter(p
							.getApellido()));
					Mesa mesa = lstMsa.get(grupo - 1);
					Voto v = new Voto(mesa, p);
					v.create();
				}
			}
		}
	}

	public String finalizarVotacion() {
		int asig = new Voto().deleteAllRegisters();
		int mesas = new Mesa().deleteAllRegisters();
		return mesas + " mesas " + asig + " padrones.";
	}

}

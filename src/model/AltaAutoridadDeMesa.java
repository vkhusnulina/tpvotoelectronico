package model;

import java.util.List;

import model.entidades.AutoridadDeMesa;
import model.entidades.Mesa;
import model.entidades.Persona;

public class AltaAutoridadDeMesa {

	private List<Mesa> lstMesas = null;
	private Persona persona = null;

	public AltaAutoridadDeMesa() {
		lstMesas = new Mesa().searchAll();
	}

	public List<Mesa> getLstMesas() {
		return lstMesas;
	}

	public Persona buscarPersonaPorDni(long dni) throws Exception {
		persona = new Persona();
		persona.setDni(dni);
		persona = persona.searchUnique();
		if (persona != null) {
			if (persona.getEdad() < 18)
				throw new Exception(
						persona.getNombre()
								+ " "
								+ persona.getApellido()
								+ " no puede ejercer cargos publicos por ser menor de edad.");
			else
				return persona;
		} else
			throw new Exception("No existe la persona con el D.N.I. " + dni);
	}

	public String altaAutoridadDeMesa(Mesa mesa) throws Exception {
		AutoridadDeMesa autoridadDeMesa = new AutoridadDeMesa(0, this.persona,
				mesa);
		autoridadDeMesa.create();
		return "Alta de AutoridadDeMesa OK!";
	}
}
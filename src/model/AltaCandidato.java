package model;

import java.util.List;

import model.entidades.Candidato;
import model.entidades.Cargo;
import model.entidades.Partido;
import model.entidades.Persona;

public class AltaCandidato {

	private List<Partido> lstPartidos = null;
	private List<Cargo> lstCargos = null;
	private Persona persona = null;

	public AltaCandidato() {
		lstCargos = new Cargo().searchAll();
		lstPartidos = new Partido().searchAll();
	}

	public List<Partido> getLstPartidos() {
		return lstPartidos;
	}

	public List<Cargo> getLstCargos() {
		return lstCargos;
	}

	public Persona buscarPersonaCandidataPorDni(long dni) throws Exception {
		persona = new Persona();
		persona.setDni(dni);
		persona = persona.searchUnique();
		if (persona != null) {
			if (persona.getIdNacionalidad() == 3
					|| persona.getIdNacionalidad() == 4)
				throw new Exception(
						persona.getNombre()
								+ " "
								+ persona.getApellido()
								+ " no puede ejercer cargos publicos por tener la nacionalidad: "
								+ persona.getNacionalidad());
			else
				return persona;
		} else
			throw new Exception("No existe la persona con el D.N.I. " + dni);
	}

	public String altaCandidato(Cargo c, Partido p, String foto)
			throws Exception {
		Candidato candidato = new Candidato(0, this.persona, p, c, foto);
		candidato.create();
		return "Alta de Candidato OK!";
	}
}

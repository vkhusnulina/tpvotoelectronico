package model;

import java.util.List;

import model.entidades.Fiscal;
import model.entidades.Mesa;
import model.entidades.Partido;
import model.entidades.Persona;

public class AltaFiscalDeMesa {

	private List<Mesa> lstMesas = null;
	private List<Partido> lstPartidos = null;
	private Persona persona = null;

	public AltaFiscalDeMesa() {
		lstMesas = new Mesa().searchAll();
		lstPartidos = new Partido().searchAll();
	}

	public List<Mesa> getLstMesas() {
		return lstMesas;
	}

	public List<Partido> getLstPartidos() {
		return lstPartidos;
	}

	public Persona buscarPersonaPorDni(long dni) throws Exception {
		persona = new Persona();
		persona.setDni(dni);
		persona = persona.searchUnique();
		if (persona != null) {
			if (persona.getEdad() < 18)
				throw new Exception(
						persona.getNombre()
								+ " "
								+ persona.getApellido()
								+ " no puede ejercer cargos publicos por ser menor de edad: ");
			else
				return persona;
		} else
			throw new Exception("No existe la persona con el D.N.I. " + dni);
	}

	public String altaFiscalDeMesa(Partido p, Mesa m) throws Exception {
		Fiscal fiscalDeMesa = new Fiscal(0, this.persona, p, m);
		fiscalDeMesa.create();
		return "Alta de FiscalDeMesa OK!";
	}
}
package model;

import model.entidades.Partido;

public class AltaPartido {

	public AltaPartido() {

	}

	public void agregar(String nombre, String url_foto, String cPrimario,
			String cSecundario) throws Exception {
		Partido partido = new Partido(nombre, url_foto, cPrimario, cSecundario);
		partido.create();
	}
}

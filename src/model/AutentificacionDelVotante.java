package model;

import java.util.List;

import model.entidades.Mesa;
import model.entidades.Persona;
import model.entidades.Voto;

public class AutentificacionDelVotante {

	public AutentificacionDelVotante() {
	}

	public int cantidadDeVotantes(Mesa mesa) {
		Voto v = new Voto();
		v.setMesa(mesa);
		List<Voto> lst = v.searchAll();
		return lst.size();
	}

	public Voto buscarVoto(Long dni, Mesa mesa) throws Exception {
		if (dni == null || dni == 0L)
			throw new Exception("Debe ingresar DNI para buscar al votante.");
		Persona p = new Persona();
		p.setDni(dni);
		p = p.searchUnique();
		if (p == null)
			throw new Exception("No existe persona con " + dni + " DNI.");
		Voto v = new Voto();
		v.setPersona(p);
		v.setMesa(mesa);
		v = v.searchUnique();
		if (v == null)
			throw new Exception(p.toString() + " no puede votar en la mesa Nro"
					+ mesa.getNumero());
		return v;
	}
}

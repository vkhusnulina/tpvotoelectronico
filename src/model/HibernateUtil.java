package model;

import model.entidades.PaisOrigen;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class HibernateUtil {

	private static final SessionFactory sessionFactory;
	private static Configuration configuration;

	static {
		try {
			configuration = new Configuration();
			addAnnotatedClasses();

			configuration.configure("/hibernate.cfg.xml");
			ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
					.applySettings(configuration.getProperties()).build();
			sessionFactory = configuration.buildSessionFactory(serviceRegistry);

		} catch (HibernateException he) {
			System.err
					.println("Ocurrió un error en la inicialización de la SessionFactory: "
							+ he);
			throw new ExceptionInInitializerError(he);
		}
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	private static void addAnnotatedClasses() {
		configuration.addAnnotatedClass(PaisOrigen.class);

	}
}

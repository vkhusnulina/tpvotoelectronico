package model;

import java.util.List;

import model.entidades.AutoridadDeMesa;
import model.entidades.Fiscal;
import model.entidades.Mesa;
import model.entidades.Persona;

public class IniciarMesa {

	public IniciarMesa() {
	}

	public AutoridadDeMesa buscarAutoridadDeMesa(Long dni) throws Exception {
		if (dni == null || dni == 0L)
			throw new Exception("Debe ingresar DNI para buscar al agente.");
		Persona p = new Persona();
		p.setDni(dni);
		p = p.searchUnique();
		if (p == null)
			throw new Exception("No existe persona con " + dni + " DNI.");
		AutoridadDeMesa a = new AutoridadDeMesa();
		a.setPersona(p);
		return a.searchUnique();
	}

	public List<AutoridadDeMesa> traerAutoridadesDeMesa(Mesa mesa) {
		AutoridadDeMesa a = new AutoridadDeMesa();
		a.setMesa(mesa);
		return a.searchAll();
	}

	public List<Fiscal> traerFiscalesDeMesa(Mesa mesa) {
		Fiscal f = new Fiscal();
		f.setMesa(mesa);
		return f.searchAll();
	}

}

package model;

import java.util.AbstractList;
import java.util.List;

public class ListUtil {
	public static <TCastTo, TCastFrom extends TCastTo> List<TCastTo> convert(
			final List<TCastFrom> list) {
		return new AbstractList<TCastTo>() {
			@Override
			public TCastTo get(int i) {
				return list.get(i);
			}

			@Override
			public int size() {
				return list.size();
			}
		};
	}

	public static <TCastTo, TCastFrom> List<TCastTo> cast(
			final List<TCastFrom> list) {
		return new AbstractList<TCastTo>() {
			@SuppressWarnings("unchecked")
			@Override
			public TCastTo get(int i) {
				return (TCastTo) list.get(i);
			}

			@Override
			public int size() {
				return list.size();
			}
		};
	}
}
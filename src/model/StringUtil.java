package model;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class StringUtil {

	private StringUtil() {
	}

	public static enum ABC {
		A, B, C, D, E, F, G, H, I, J, K, L, M, N, �, O, P, Q, R, S, T, U, V, W, X, Y, Z
	}

	public static ABC getFirstLetter(String word) {
		String letter = word.substring(0, 1);
		return ABC.valueOf(letter.toUpperCase());
	}

	public static String GregorianCalendarToString(GregorianCalendar gc) {
		return gc.get(Calendar.DAY_OF_MONTH) + "/"
				+ (gc.get(Calendar.MONTH) + 1) + "/" + gc.get(Calendar.YEAR);
	}
}

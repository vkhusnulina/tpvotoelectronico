package model.dao;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import model.HibernateUtil;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

public class EntityDAO {
	private Session sesion;
	private Transaction tx;

	protected void save(Object entity) throws HibernateException {

		try {
			iniciaOperacion();
			sesion.save(entity);
			tx.commit();
		} catch (HibernateException he) {
			manejaExcepcion(he);
			throw he;
		} finally {
			sesion.flush();
			sesion.close();
		}

	}

	protected void update(Object entity) throws HibernateException {

		try {
			iniciaOperacion();
			sesion.update(entity);
			tx.commit();
		} catch (HibernateException he) {
			manejaExcepcion(he);
			throw he;
		} finally {
			sesion.flush();
			sesion.close();
		}

	}

	protected void delete(Object entity) throws HibernateException {

		try {
			iniciaOperacion();
			sesion.delete(entity);
			tx.commit();
		} catch (HibernateException he) {
			manejaExcepcion(he);
			throw he;
		} finally {
			sesion.flush();
			sesion.close();
		}

	}

	protected <T> Object getUniqueResult(T testObject) {
		iniciaOperacion();
		Criteria cr = setCriteriaQuery(testObject, 1);
		Object obj = cr.uniqueResult();
		sesion.flush();
		sesion.close();
		return obj;
	}

	protected <T> List<Object> getList(T testObject) {
		iniciaOperacion();
		Criteria cr = setCriteriaQuery(testObject, 0);
		List<?> lst = cr.list();
		List<Object> lstobj = new ArrayList<Object>();
		lstobj.addAll(lst);
		sesion.flush();
		sesion.close();
		return lstobj;
	}

	protected <T> int cleanTable(T testObject) {
		iniciaOperacion();
		String sql = String.format("delete from %s", getTableName(testObject));
		int n = sesion.createSQLQuery(sql).executeUpdate();
		sesion.close();
		return n;
	}

	protected List<Object> getByHQL(String hql) {
		iniciaOperacion();
		Query query = sesion.createQuery(hql);
		List<Object> lst = query.list();
		sesion.flush();
		sesion.close();
		return lst;
	}

	private <T> Criteria setCriteriaQuery(T testObject, int maxResults) {
		Criteria cr = sesion.createCriteria(testObject.getClass());
		if (maxResults > 0) {
			cr.setMaxResults(maxResults);
		}

		for (Field field : testObject.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			Object value;
			try {
				value = field.get(testObject);
				// si es null el valor no se agrega a la query
				if (value != null && valueIsIdAndDifferent0(field, testObject)) {
					String property = getColumnName(field);
					cr.add(Restrictions.like(property, value));
				}
			} catch (IllegalArgumentException | IllegalAccessException e) {
			}
		}
		return cr;
	}

	private <T> boolean valueIsIdAndDifferent0(Field field, T testObject) {
		if (field.isAnnotationPresent(Id.class)) {
			try {
				return field.getInt(testObject) != 0;
			} catch (IllegalArgumentException e) {
				return true;
			} catch (IllegalAccessException e) {
				return true;
			}
		}
		return true;
	}

	private String getColumnName(Field field) {
		if (field.isAnnotationPresent(Column.class)) {
			Column column = field.getAnnotation(Column.class);
			if (column.name() != null) {
				return column.name();
			}
		}
		return field.getName();
	}

	private <T> String getTableName(T testObject) {
		if (testObject.getClass().isAnnotationPresent(Table.class)) {
			Table table = testObject.getClass().getAnnotation(Table.class);
			return table.name();
		}
		return testObject.getClass().getName();
	}

	private void iniciaOperacion() throws HibernateException {
		sesion = HibernateUtil.getSessionFactory().openSession();
		tx = sesion.beginTransaction();
		tx.setTimeout(new Integer(10));
	}

	private void manejaExcepcion(HibernateException he)
			throws HibernateException {
		tx.rollback();
		throw new HibernateException(
				"Ocurri� un error en la capa de acceso a datos\n"
						+ he.getMessage());
	}

}

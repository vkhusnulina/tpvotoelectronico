package model.entidades;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "autoridades_de_mesa")
public class AutoridadDeMesa extends MyEntity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@OneToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "id_persona", insertable = true, updatable = true, nullable = false, unique = true)
	private Persona persona;
	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "id_mesa", insertable = true, updatable = true, nullable = false, unique = true)
	private Mesa mesa;

	public AutoridadDeMesa() {
	}

	public AutoridadDeMesa(int id, Persona persona, Mesa mesa) {
		this.id = id;
		this.persona = persona;
		this.mesa = mesa;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public Mesa getMesa() {
		return mesa;
	}

	public void setMesa(Mesa mesa) {
		this.mesa = mesa;
	}

}

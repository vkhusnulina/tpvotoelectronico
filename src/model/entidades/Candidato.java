package model.entidades;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "candidatos")
public class Candidato extends MyEntity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@OneToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "id_persona", insertable = true, updatable = true, nullable = false, unique = true)
	private Persona persona;
	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "id_partido", insertable = true, updatable = true, nullable = false, unique = false)
	private Partido partido;
	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "id_cargo", insertable = true, updatable = true, nullable = false, unique = false)
	private Cargo cargo;
	private String url_foto;

	public Candidato() {
	}

	public Candidato(int id, Persona persona, Partido partido, Cargo cargo,
			String url_foto) {
		this.id = id;
		this.persona = persona;
		this.partido = partido;
		this.cargo = cargo;
		this.url_foto = url_foto;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public Partido getPartido() {
		return partido;
	}

	public void setPartido(Partido partido) {
		this.partido = partido;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	public String getUrl_foto() {
		return url_foto;
	}

	public void setUrl_foto(String url_foto) {
		this.url_foto = url_foto;
	}

}

package model.entidades;

import java.io.Serializable;
import java.util.GregorianCalendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "candidatos_votos")
public class CandidatoVoto extends MyEntity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	@OneToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "id_candidato", insertable = true, updatable = true, nullable = false, unique = true)
	private Candidato candidato;
	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "id_mesa", insertable = true, updatable = true, nullable = false, unique = true)
	private Mesa mesa;
	@Column(columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private GregorianCalendar fecha;
	@Column(columnDefinition = "int default '0'")
	private Integer votos;

	public CandidatoVoto() {
	}

	public CandidatoVoto(int id, Candidato candidato, Mesa mesa,
			GregorianCalendar fecha, int votos) {
		this.id = id;
		this.candidato = candidato;
		this.mesa = mesa;
		this.fecha = fecha;
		this.votos = votos;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Candidato getCandidato() {
		return candidato;
	}

	public void setCandidato(Candidato candidato) {
		this.candidato = candidato;
	}

	public Mesa getMesa() {
		return mesa;
	}

	public void setMesa(Mesa mesa) {
		this.mesa = mesa;
	}

	public GregorianCalendar getFecha() {
		return fecha;
	}

	public void setFecha(GregorianCalendar fecha) {
		this.fecha = fecha;
	}

	public int getVotos() {
		return votos;
	}

	public void setVotos(int votos) {
		this.votos = votos;
	}

}

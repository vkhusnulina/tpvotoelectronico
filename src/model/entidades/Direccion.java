package model.entidades;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "direcciones")
public class Direccion extends MyEntity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "id_localidad", insertable = true, updatable = true, nullable = false)
	private Localidad localidad;
	private Integer codigo_postal;
	private String calle;
	private Integer altura;
	private String departamento;

	public Direccion() {
	}

	public Direccion(int id, Localidad localidad, int codigo_postal,
			String calle, int altura, String departamento) {
		this.id = id;
		this.localidad = localidad;
		this.codigo_postal = codigo_postal;
		this.calle = calle;
		this.altura = altura;
		this.departamento = departamento;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Localidad getLocalidad() {
		return localidad;
	}

	public void setLocalidad(Localidad localidad) {
		this.localidad = localidad;
	}

	public int getCodigo_postal() {
		return codigo_postal;
	}

	public void setCodigo_postal(int codigo_postal) {
		this.codigo_postal = codigo_postal;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public int getAltura() {
		return altura;
	}

	public void setAltura(int altura) {
		this.altura = altura;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	@Override
	public String toString() {
		return localidad.getDepartamento().getProvincia().getNombre() + ", "
				+ localidad.getDepartamento().getNombre() + ", "
				+ localidad.getNombre() + ", " + calle + ", " + altura
				+ ", dep." + departamento + ", CP" + codigo_postal;
	}

}

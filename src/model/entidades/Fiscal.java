package model.entidades;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "fiscales")
public class Fiscal extends MyEntity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@OneToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "id_persona", insertable = true, updatable = true, nullable = false, unique = true)
	private Persona persona;
	@OneToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "id_partido", insertable = true, updatable = true, nullable = false, unique = true)
	private Partido partido;
	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "id_mesa", insertable = true, updatable = true, nullable = false)
	private Mesa mesa;

	public Fiscal() {
	}

	public Fiscal(int id, Persona persona, Partido partido, Mesa mesa) {
		this.id = id;
		this.persona = persona;
		this.partido = partido;
		this.mesa = mesa;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public Partido getPartido() {
		return partido;
	}

	public void setPartido(Partido partido) {
		this.partido = partido;
	}

	public Mesa getMesa() {
		return mesa;
	}

	public void setMesa(Mesa mesa) {
		this.mesa = mesa;
	}

}

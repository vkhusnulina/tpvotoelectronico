package model.entidades;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "mesas")
public class Mesa extends MyEntity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@ManyToOne()
	@JoinColumn(name = "id_institucion", insertable = true, updatable = true, nullable = false)
	private Institucion institucion;
	@Column(nullable = false, unique = true)
	private Integer numero;

	public Mesa() {
	}

	public Mesa(int id, Institucion institucion, int numero) {
		this.id = id;
		this.institucion = institucion;
		this.numero = numero;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Institucion getInstitucion() {
		return institucion;
	}

	public void setInstitucion(Institucion institucion) {
		this.institucion = institucion;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	@Override
	public String toString() {
		return "Mesa N�" + numero + " de " + institucion.getNombre()
				+ " institucion.";
	}

}

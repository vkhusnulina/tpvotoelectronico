package model.entidades;

import java.lang.reflect.Field;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

import model.ListUtil;
import model.dao.EntityDAO;

public class MyEntity extends EntityDAO {

	protected MyEntity() {
	}

	public void create() throws Exception {
		if (verifyNullFields(this))
			this.save(this);
		else
			throw new Exception("Hay valores obligatorios faltantes");
	}

	public void actualize() {
		this.update(this);
	}

	public void searchAndDelete() {
		this.delete(this.getUniqueResult(this));
	}

	public <T> T searchUnique() {
		return (T) this.getUniqueResult(this);
	}

	public <T> List<T> searchAll() {
		return ListUtil.cast(this.getList(this));
	}

	public <T> List<T> searchByHQL(String hql) {
		return ListUtil.cast(this.getByHQL(hql));
	}

	public int deleteAllRegisters() {
		List<MyEntity> list = searchAll();
		for (MyEntity o : list) {
			this.delete(o);
		}
		return list.size();
	}

	private <T> boolean verifyNullFields(T myObject) {
		try {
			for (Field field : myObject.getClass().getDeclaredFields()) {
				field.setAccessible(true);
				Object value;
				if (!field.isAnnotationPresent(Id.class)) {
					if (!getAnnotationNullableValue(field)) {
						value = field.get(myObject);
						// si es null retorna false, ya que dicho campo no
						// permite
						// valores nulos
						if (value == null || value.equals("")) {
							return false;
						}
					}
				}
			}
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return true;
	}

	private boolean getAnnotationNullableValue(Field field) {
		if (field.isAnnotationPresent(Column.class)) {
			Column column = field.getAnnotation(Column.class);
			return column.nullable();
		} else if (field.isAnnotationPresent(JoinColumn.class)) {
			JoinColumn column = field.getAnnotation(JoinColumn.class);
			return column.nullable();
		}
		return false;

	}

}

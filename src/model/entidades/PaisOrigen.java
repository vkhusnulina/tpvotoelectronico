package model.entidades;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "paises_origen")
public class PaisOrigen extends MyEntity implements Serializable {

	@Id
	@Column(name = "iso")
	private String iso;
	private String nombre;

	public PaisOrigen() {
	}

	public PaisOrigen(String iso, String nombre) {
		this.setIso(iso);
		this.setNombre(nombre);
	}

	public String getIso() {
		return iso;
	}

	public void setIso(String iso) {
		this.iso = iso;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}

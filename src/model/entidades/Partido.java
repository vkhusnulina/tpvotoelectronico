package model.entidades;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "partidos")
public class Partido extends MyEntity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@Column(nullable = false)
	private String nombre = null;
	@Column(columnDefinition = "varchar(50)")
	private String url_foto = null;
	@Column(columnDefinition = "varchar(7) default '#FFFFFF'")
	private String color_hexa_primario;
	@Column(columnDefinition = "varchar(7) default '#000000'")
	private String color_hexa_secundario;

	public Partido() {
	}

	public Partido(String nombre, String url_foto, String color_hexa_primario,
			String color_hexa_secundario) {
		this.nombre = nombre;
		this.url_foto = url_foto;
		this.color_hexa_primario = color_hexa_primario;
		this.color_hexa_secundario = color_hexa_secundario;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUrl_foto() {
		return url_foto;
	}

	public void setUrl_foto(String url_foto) {
		this.url_foto = url_foto;
	}

	public String getColor_hexa_primario() {
		return color_hexa_primario;
	}

	public void setColor_hexa_primario(String color_hexa_primario) {
		this.color_hexa_primario = color_hexa_primario;
	}

	public String getColor_hexa_secundario() {
		return color_hexa_secundario;
	}

	public void setColor_hexa_secundario(String color_hexa_secundario) {
		this.color_hexa_secundario = color_hexa_secundario;
	}

	@Override
	public String toString() {
		return nombre;
	}
}

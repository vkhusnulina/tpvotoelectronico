package model.entidades;

import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "personas")
public class Persona extends MyEntity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@OneToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "id_direccion", insertable = true, updatable = true, nullable = false, unique = true)
	private Direccion direccion;

	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "id_pais_origen", insertable = true, updatable = true, nullable = false)
	private PaisOrigen pais_origen;

	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "id_nacionalidad", insertable = true, updatable = true, nullable = false)
	private Nacionalidad nacionalidad;

	@Column(name = "dni", unique = true, nullable = false)
	private Long dni;
	@Column(columnDefinition = "varchar(11) default null")
	private String cuil;
	private String nombre;
	private String apellido;
	private GregorianCalendar fecha_nacimiento;
	@Column(columnDefinition = "varchar(1) default null")
	private Character sexo;
	@Column(columnDefinition = "varchar(1) default null")
	private Character estado_civil;
	private String url_foto;

	// @OneToMany(mappedBy = "persona")
	// private Set<Administrador> administradores;

	public Persona() {
	}

	public Persona(int id, Direccion direccion, PaisOrigen pais_origen,
			Nacionalidad nacionalidad, long dni, String cuil, String nombre,
			String apellido, GregorianCalendar fecha_nacimiento, char sexo,
			char estado_civil, String url_foto) {
		this.id = id;
		this.direccion = direccion;
		this.pais_origen = pais_origen;
		this.nacionalidad = nacionalidad;
		this.dni = dni;
		this.cuil = cuil;
		this.nombre = nombre;
		this.apellido = apellido;
		this.fecha_nacimiento = fecha_nacimiento;
		this.sexo = sexo;
		this.estado_civil = estado_civil;
		this.url_foto = url_foto;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Direccion getDireccion() {
		return direccion;
	}

	public void setDireccion(Direccion direccion) {
		this.direccion = direccion;
	}

	public String getPais_origen() {
		return pais_origen.getNombre();
	}

	public void setPais_origen(PaisOrigen pais_origen) {
		this.pais_origen = pais_origen;
	}

	public String getNacionalidad() {
		return nacionalidad.getNombre();
	}

	public int getIdNacionalidad() {
		return nacionalidad.getId();
	}

	public void setNacionalidad(Nacionalidad nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	public long getDni() {
		return dni;
	}

	public void setDni(long dni) {
		this.dni = dni;
	}

	public String getCuil() {
		return cuil;
	}

	public void setCuil(String cuil) {
		this.cuil = cuil;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public GregorianCalendar getFecha_nacimiento() {
		return fecha_nacimiento;
	}

	public void setFecha_nacimiento(GregorianCalendar fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}

	public String getSexo() {
		if (sexo == 'M')
			return "Masculino";
		else
			return "Femenino";
	}

	public void setSexo(char sexo) {
		this.sexo = sexo;
	}

	public char getEstado_civil() {
		return estado_civil;
	}

	public void setEstado_civil(char estado_civil) {
		this.estado_civil = estado_civil;
	}

	public String getUrl_foto() {
		return url_foto;
	}

	public void setUrl_foto(String url_foto) {
		this.url_foto = url_foto;
	}

	public int getEdad() {
		Calendar birth = getFecha_nacimiento();
		Calendar today = new GregorianCalendar();
		int age = 0;
		int factor = 0;
		if (today.get(Calendar.MONTH) <= birth.get(Calendar.MONTH)) {
			if (today.get(Calendar.MONTH) == birth.get(Calendar.MONTH)) {
				if (today.get(Calendar.DATE) > birth.get(Calendar.DATE)) {
					factor = -1; // Aun no celebra su cumpleaños
				}
			} else {
				factor = -1; // Aun no celebra su cumpleaños
			}
		}
		age = (today.get(Calendar.YEAR) - birth.get(Calendar.YEAR)) + factor;
		return age;
	}

	public List<Persona> buscarPersonasPorLocalidadId(int lcl) {
		// String hql =
		// "FROM Persona P, Direccion D WHERE P.direccion.id = D.id AND D.localidad.id = '"
		// + lcl + "'";
		String hql = "FROM Persona P WHERE P.direccion.localidad.id = '" + lcl
				+ "'";
		return this.searchByHQL(hql);
	}

	@Override
	public String toString() {
		return nombre.toUpperCase() + ", " + apellido.toUpperCase();
	}

}

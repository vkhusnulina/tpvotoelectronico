package model.entidades;

import java.io.Serializable;
import java.util.GregorianCalendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "votos")
public class Voto extends MyEntity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "id_mesa", insertable = true, updatable = true, nullable = false)
	private Mesa mesa;
	@OneToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "id_persona", insertable = true, updatable = true, nullable = false, unique = true)
	private Persona persona;
	@Column(columnDefinition = "DATETIME default '1000-01-01 00:00:00'")
	private GregorianCalendar fecha;
	@Column(columnDefinition = "int default 0")
	private Integer voto_realizado;

	public Voto() {
	}

	public Voto(int id, Mesa mesa, Persona persona, GregorianCalendar fecha,
			int voto_realizado) {
		this.id = id;
		this.mesa = mesa;
		this.persona = persona;
		this.fecha = fecha;
		this.voto_realizado = voto_realizado;
	}

	public Voto(Mesa mesa, Persona persona) {
		this.mesa = mesa;
		this.persona = persona;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Mesa getMesa() {
		return mesa;
	}

	public void setMesa(Mesa mesa) {
		this.mesa = mesa;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public GregorianCalendar getFecha() {
		return fecha;
	}

	public void setFecha(GregorianCalendar fecha) {
		this.fecha = fecha;
	}

	public int getVoto_realizado() {
		return voto_realizado;
	}

	public String getVotoRealizado() {
		if (voto_realizado == 0)
			return "NO";
		else
			return "SI";
	}

	public void setVoto_realizado(int voto_realizado) {
		this.voto_realizado = voto_realizado;
	}

}

package view;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class AdministracionGUI extends JFrame {

	private JButton altaPersona = new JButton("Alta Persona");
	private JButton altaPartido = new JButton("Alta Partido");
	private JButton altaInstitucion = new JButton("Alta Institucion");

	private JButton asignarMesas = new JButton("Asignar Mesas");

	private JButton altaAutoridadDeMesa = new JButton("Alta Autoridad De Mesa");
	private JButton altaFiscalDeMesa = new JButton("Alta Fiscal De Mesa");
	private JButton altaCandidato = new JButton("Alta Candidato");

	private JButton iniciarVotacion = new JButton("Iniciar Votacion");
	private JButton finalizarVotacion = new JButton("Finalizar Votacion");

	public AdministracionGUI() {

		// Sets up the view and adds the components

		JPanel panel = new JPanel();

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(600, 200);

		panel.add(altaPersona);
		panel.add(altaPartido);
		panel.add(altaInstitucion);

		panel.add(asignarMesas);

		panel.add(altaAutoridadDeMesa);
		panel.add(altaFiscalDeMesa);
		panel.add(altaCandidato);

		panel.add(iniciarVotacion);
		panel.add(finalizarVotacion);

		this.add(panel);

		// End of setting up the components --------
	}

	public void altaPersonaListener(ActionListener listen) {
		altaPersona.addActionListener(listen);
	}

	public void altaPartidoListener(ActionListener listen) {
		altaPartido.addActionListener(listen);
	}

	public void altaInstitucionListener(ActionListener listen) {
		altaInstitucion.addActionListener(listen);
	}

	public void asignarMesasListener(ActionListener listen) {
		asignarMesas.addActionListener(listen);
	}

	public void altaAutoridadDeMesaListener(ActionListener listen) {
		altaAutoridadDeMesa.addActionListener(listen);
	}

	public void altaFiscalDeMesaListener(ActionListener listen) {
		altaFiscalDeMesa.addActionListener(listen);
	}

	public void altaCandidatoListener(ActionListener listen) {
		altaCandidato.addActionListener(listen);
	}

	public void iniciarVotacionListener(ActionListener listen) {
		iniciarVotacion.addActionListener(listen);
	}

	public void finalizarVotacionListener(ActionListener listen) {
		finalizarVotacion.addActionListener(listen);
	}

	public void displayErrorMessage(String errorMessage) {
		JOptionPane.showMessageDialog(this, errorMessage, "Error",
				JOptionPane.ERROR_MESSAGE);
	}

	public void displayInfoMessage(String infoMessage) {
		JOptionPane.showMessageDialog(this, infoMessage, "",
				JOptionPane.INFORMATION_MESSAGE);
	}
}

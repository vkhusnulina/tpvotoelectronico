package view;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class LogInGUI extends JFrame {

	private JTextField txtUser = new JTextField(10);
	private JTextField txtPassword = new JTextField(10);
	private JButton btnEnter = new JButton("OK");

	public LogInGUI() {

		// Sets up the view and adds the components

		JPanel logInPanel = new JPanel();

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(600, 200);

		logInPanel.add(txtUser);
		logInPanel.add(txtPassword);
		logInPanel.add(btnEnter);

		this.add(logInPanel);

		// End of setting up the components --------

	}

	public String getUser() {

		return txtUser.getText();

	}

	public String getPassword() {

		return txtPassword.getText();

	}

	public void addLogInListener(ActionListener listenForOkButton) {

		btnEnter.addActionListener(listenForOkButton);

	}

	// Open a popup that contains the message passed

	public void displayErrorMessage(String errorMessage) {
		JOptionPane.showMessageDialog(this, errorMessage, "Error",
				JOptionPane.ERROR_MESSAGE);
	}

	public void displayWarningMessage(String wrngMessage) {
		JOptionPane.showMessageDialog(this, wrngMessage, "Aviso",
				JOptionPane.WARNING_MESSAGE);
	}

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package view.altas;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;

import model.entidades.Cargo;
import model.entidades.Partido;

public class AltaCandidatoGUI extends JFrame {

	// Variables declaration - do not modify
	private JButton btnBuscar = new JButton("Buscar");
	private JButton btnCrear = new JButton("Crear");
	private final JButton btnVolver = new JButton("Volver");
	private JComboBox cmbCargo = new JComboBox();
	private JComboBox cmbPartido = new JComboBox();
	private JLabel lbl1 = new JLabel("Nuevo Candidato");
	private JLabel lbl2 = new JLabel("Buscar por D.N.I.");
	private JLabel lbl3 = new JLabel("Nombre:");
	private JLabel lbl4 = new JLabel("Apellido:");
	private JLabel lbl5 = new JLabel("Sexo:");
	private JLabel lbl6 = new JLabel("Nacionalidad:");
	private JLabel lbl7 = new JLabel("Partido:");
	private JLabel lbl8 = new JLabel("Cargo:");
	private JLabel lblAvatar = new JLabel();
	private JSeparator spr1 = new JSeparator();
	private JTextField txtDni = new JTextField(8);
	private Font lblFont = new Font("SansSerif", 1, 14);

	private JLabel lblNewLabel = new JLabel("");

	// End of variables declaration

	public AltaCandidatoGUI() {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		initComponents();
	}

	public void setNombre(String txt) {
		this.lbl3.setText("Nombre: " + txt);
	}

	public void setApellido(String txt) {
		this.lbl4.setText("Apellido: " + txt);
	}

	public void setSexo(String txt) {
		this.lbl5.setText("Sexo: " + txt);
	}

	public void setNacionalidad(String txt) {
		this.lbl6.setText("Nacionalidad: " + txt);
	}

	public void setCargos(List<Cargo> lstcargos) {
		for (Cargo c : lstcargos) {
			this.cmbCargo.addItem(c);
		}
	}

	public void setPartidos(List<Partido> lstp) {
		for (Partido c : lstp) {
			this.cmbPartido.addItem(c);
		}
	}

	public Cargo getCargo() {
		return (Cargo) this.cmbCargo.getSelectedItem();
	}

	public Partido getPartido() {
		return (Partido) this.cmbPartido.getSelectedItem();
	}

	public String getDni() {
		return txtDni.getText();
	}

	public void buscarListener(ActionListener listen) {
		btnBuscar.addActionListener(listen);
	}

	public void crearListener(ActionListener listen) {
		btnCrear.addActionListener(listen);
	}

	public void volverListener(ActionListener listen) {
		btnVolver.addActionListener(listen);
	}

	public void displayErrorMessage(String errorMessage) {
		JOptionPane.showMessageDialog(this, errorMessage, "Error",
				JOptionPane.ERROR_MESSAGE);
	}

	public void displayWarningMessage(String wrngMessage) {
		JOptionPane.showMessageDialog(this, wrngMessage, "Aviso",
				JOptionPane.WARNING_MESSAGE);
	}

	public void displayInfoMessage(String infoMessage) {
		JOptionPane.showMessageDialog(this, infoMessage, "",
				JOptionPane.INFORMATION_MESSAGE);
	}

	private void initComponents() {
		lbl1.setFont(new java.awt.Font("SansSerif", 0, 24));
		lbl2.setFont(lblFont);
		lbl3.setFont(lblFont);
		lbl4.setFont(lblFont);
		lbl5.setFont(lblFont);
		lbl6.setFont(lblFont);
		lbl7.setFont(lblFont);
		lbl8.setFont(lblFont);
		cmbCargo.addItem(null);
		cmbPartido.addItem(null);

		GroupLayout layout = new GroupLayout(getContentPane());
		lblNewLabel = new JLabel("");
		layout.setHorizontalGroup(layout
				.createParallelGroup(Alignment.LEADING)
				.addGroup(
						layout.createSequentialGroup()
								.addGroup(
										layout.createParallelGroup(
												Alignment.TRAILING)
												.addGroup(
														layout.createSequentialGroup()
																.addGroup(
																		layout.createParallelGroup(
																				Alignment.LEADING)
																				.addGroup(
																						layout.createSequentialGroup()
																								.addContainerGap()
																								.addComponent(
																										lbl1))
																				.addGroup(
																						layout.createSequentialGroup()
																								.addGap(205)
																								.addGroup(
																										layout.createParallelGroup(
																												Alignment.TRAILING)
																												.addGroup(
																														layout.createSequentialGroup()
																																.addComponent(
																																		lblNewLabel,
																																		GroupLayout.PREFERRED_SIZE,
																																		0,
																																		GroupLayout.PREFERRED_SIZE)
																																.addGap(357))
																												.addGroup(
																														layout.createParallelGroup(
																																Alignment.LEADING)
																																.addComponent(
																																		lbl2)
																																.addGroup(
																																		layout.createSequentialGroup()
																																				.addComponent(
																																						txtDni,
																																						GroupLayout.PREFERRED_SIZE,
																																						280,
																																						GroupLayout.PREFERRED_SIZE)
																																				.addPreferredGap(
																																						ComponentPlacement.RELATED)
																																				.addComponent(
																																						btnBuscar))
																																.addGroup(
																																		layout.createSequentialGroup()
																																				.addComponent(
																																						lblAvatar,
																																						GroupLayout.PREFERRED_SIZE,
																																						150,
																																						GroupLayout.PREFERRED_SIZE)
																																				.addGap(18)
																																				.addGroup(
																																						layout.createParallelGroup(
																																								Alignment.LEADING)
																																								.addComponent(
																																										lbl4)
																																								.addComponent(
																																										lbl3)
																																								.addComponent(
																																										lbl5)
																																								.addComponent(
																																										lbl6)))
																																.addComponent(
																																		lbl7)
																																.addComponent(
																																		cmbPartido,
																																		GroupLayout.PREFERRED_SIZE,
																																		273,
																																		GroupLayout.PREFERRED_SIZE)
																																.addComponent(
																																		lbl8)
																																.addComponent(
																																		cmbCargo,
																																		GroupLayout.PREFERRED_SIZE,
																																		273,
																																		GroupLayout.PREFERRED_SIZE))
																												.addGroup(
																														layout.createSequentialGroup()
																																.addComponent(
																																		btnVolver)
																																.addPreferredGap(
																																		ComponentPlacement.UNRELATED)
																																.addComponent(
																																		btnCrear)))))
																.addGap(0,
																		164,
																		Short.MAX_VALUE))
												.addGroup(
														layout.createSequentialGroup()
																.addContainerGap()
																.addComponent(
																		spr1,
																		GroupLayout.PREFERRED_SIZE,
																		GroupLayout.DEFAULT_SIZE,
																		GroupLayout.PREFERRED_SIZE)))
								.addContainerGap()));
		layout.setVerticalGroup(layout
				.createParallelGroup(Alignment.LEADING)
				.addGroup(
						layout.createSequentialGroup()
								.addContainerGap()
								.addComponent(lbl1)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(spr1, GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addGap(43)
								.addComponent(lbl2)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addGroup(
										layout.createParallelGroup(
												Alignment.BASELINE)
												.addComponent(
														txtDni,
														GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE,
														GroupLayout.PREFERRED_SIZE)
												.addComponent(btnBuscar))
								.addGap(18)
								.addComponent(lblNewLabel,
										GroupLayout.PREFERRED_SIZE, 0,
										GroupLayout.PREFERRED_SIZE)
								.addGroup(
										layout.createParallelGroup(
												Alignment.LEADING)
												.addGroup(
														layout.createSequentialGroup()
																.addGap(18)
																.addComponent(
																		lblAvatar,
																		GroupLayout.PREFERRED_SIZE,
																		150,
																		GroupLayout.PREFERRED_SIZE)
																.addPreferredGap(
																		ComponentPlacement.RELATED)
																.addComponent(
																		lbl7))
												.addGroup(
														layout.createSequentialGroup()
																.addGap(19)
																.addComponent(
																		lbl3)
																.addPreferredGap(
																		ComponentPlacement.RELATED)
																.addComponent(
																		lbl4)
																.addPreferredGap(
																		ComponentPlacement.RELATED)
																.addComponent(
																		lbl5)
																.addPreferredGap(
																		ComponentPlacement.RELATED)
																.addComponent(
																		lbl6)))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(cmbPartido,
										GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addComponent(lbl8)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(cmbCargo,
										GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addGroup(
										layout.createParallelGroup(
												Alignment.BASELINE)
												.addComponent(btnCrear)
												.addComponent(btnVolver))
								.addContainerGap(16, Short.MAX_VALUE)));
		getContentPane().setLayout(layout);
		pack();
	}
}

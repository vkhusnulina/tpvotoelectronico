/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package view.altas;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.WindowConstants;

import model.entidades.Mesa;
import model.entidades.Partido;

public class AltaFiscalDeMesaGUI extends JFrame {

	public AltaFiscalDeMesaGUI() {
		initComponents();
	}

	@SuppressWarnings("unchecked")
	// <editor-fold defaultstate="collapsed"
	// desc="Generated Code">//GEN-BEGIN:initComponents
	private void initComponents() {

		lblNombre = new JLabel();
		jButton_seleccionar = new JButton();
		lblApellido = new JLabel();
		jLabel1 = new JLabel();
		lblNacionalidad = new JLabel();
		jSeparator1 = new JSeparator();
		jLabel6 = new JLabel();
		jLabel2 = new JLabel();
		jTextField_dni = new JTextField();
		jButton_buscar = new JButton();
		jLabel_avatar = new JLabel();
		jLabel7 = new JLabel();
		jComboBox_partido = new JComboBox();
		jComboBox_mesa = new JComboBox();

		jComboBox_partido.addItem(null);
		jComboBox_mesa.addItem(null);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		lblNombre.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
		lblNombre.setText("Nombre:");

		jButton_seleccionar.setText("Agregar");

		lblApellido.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
		lblApellido.setText("Apellido:");

		jLabel1.setFont(new java.awt.Font("SansSerif", 0, 24)); // NOI18N
		jLabel1.setText("Seleccion de Fiscal de Mesa");

		lblNacionalidad.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
		lblNacionalidad.setText("Sexo:");

		jLabel6.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
		jLabel6.setText("Nacionalidad:");

		jLabel2.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
		jLabel2.setText("Buscar por D.N.I.");

		jButton_buscar.setText("Buscar");

		// jLabel_avatar
		// .setIcon(new ImageIcon(
		// "C:\\Users\\Euen\\Pictures\\Sample Pictures\\profile_default.png"));
		// // NOI18N

		jLabel7.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
		jLabel7.setText("Partido:");

		JLabel lblMesa = new JLabel();
		lblMesa.setText("Mesa:");
		lblMesa.setFont(new Font("SansSerif", Font.BOLD, 14));

		btnVolver = new JButton("Volver");

		GroupLayout layout = new GroupLayout(getContentPane());
		layout.setHorizontalGroup(layout
				.createParallelGroup(Alignment.LEADING)
				.addGroup(
						layout.createSequentialGroup()
								.addGroup(
										layout.createParallelGroup(
												Alignment.TRAILING)
												.addGroup(
														layout.createSequentialGroup()
																.addGroup(
																		layout.createParallelGroup(
																				Alignment.LEADING)
																				.addGroup(
																						layout.createSequentialGroup()
																								.addContainerGap()
																								.addComponent(
																										jLabel1))
																				.addGroup(
																						layout.createSequentialGroup()
																								.addGap(175)
																								.addGroup(
																										layout.createParallelGroup(
																												Alignment.TRAILING)
																												.addGroup(
																														layout.createParallelGroup(
																																Alignment.LEADING)
																																.addComponent(
																																		jLabel2)
																																.addGroup(
																																		layout.createSequentialGroup()
																																				.addComponent(
																																						jTextField_dni,
																																						GroupLayout.PREFERRED_SIZE,
																																						280,
																																						GroupLayout.PREFERRED_SIZE)
																																				.addPreferredGap(
																																						ComponentPlacement.RELATED)
																																				.addComponent(
																																						jButton_buscar))
																																.addGroup(
																																		layout.createSequentialGroup()
																																				.addComponent(
																																						jLabel_avatar,
																																						GroupLayout.PREFERRED_SIZE,
																																						150,
																																						GroupLayout.PREFERRED_SIZE)
																																				.addGap(18)
																																				.addGroup(
																																						layout.createParallelGroup(
																																								Alignment.LEADING)
																																								.addComponent(
																																										lblApellido)
																																								.addComponent(
																																										lblNombre)
																																								.addComponent(
																																										lblNacionalidad)
																																								.addComponent(
																																										jLabel6)))
																																.addComponent(
																																		jLabel7)
																																.addComponent(
																																		jComboBox_partido,
																																		GroupLayout.PREFERRED_SIZE,
																																		273,
																																		GroupLayout.PREFERRED_SIZE)
																																.addComponent(
																																		lblMesa,
																																		GroupLayout.PREFERRED_SIZE,
																																		54,
																																		GroupLayout.PREFERRED_SIZE)
																																.addComponent(
																																		jComboBox_mesa,
																																		GroupLayout.PREFERRED_SIZE,
																																		273,
																																		GroupLayout.PREFERRED_SIZE))
																												.addGroup(
																														layout.createSequentialGroup()
																																.addComponent(
																																		btnVolver)
																																.addPreferredGap(
																																		ComponentPlacement.RELATED)
																																.addComponent(
																																		jButton_seleccionar)))))
																.addGap(0,
																		164,
																		Short.MAX_VALUE))
												.addGroup(
														layout.createSequentialGroup()
																.addContainerGap()
																.addComponent(
																		jSeparator1,
																		GroupLayout.PREFERRED_SIZE,
																		GroupLayout.DEFAULT_SIZE,
																		GroupLayout.PREFERRED_SIZE)))
								.addContainerGap()));
		layout.setVerticalGroup(layout
				.createParallelGroup(Alignment.LEADING)
				.addGroup(
						layout.createSequentialGroup()
								.addContainerGap()
								.addComponent(jLabel1)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(jSeparator1,
										GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addGap(43)
								.addComponent(jLabel2)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addGroup(
										layout.createParallelGroup(
												Alignment.BASELINE)
												.addComponent(
														jTextField_dni,
														GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE,
														GroupLayout.PREFERRED_SIZE)
												.addComponent(jButton_buscar))
								.addGap(33)
								.addGroup(
										layout.createParallelGroup(
												Alignment.LEADING)
												.addGroup(
														layout.createSequentialGroup()
																.addGap(18)
																.addComponent(
																		jLabel_avatar,
																		GroupLayout.PREFERRED_SIZE,
																		150,
																		GroupLayout.PREFERRED_SIZE)
																.addPreferredGap(
																		ComponentPlacement.RELATED)
																.addComponent(
																		jLabel7))
												.addGroup(
														layout.createSequentialGroup()
																.addGap(19)
																.addComponent(
																		lblNombre)
																.addPreferredGap(
																		ComponentPlacement.RELATED)
																.addComponent(
																		lblApellido)
																.addPreferredGap(
																		ComponentPlacement.RELATED)
																.addComponent(
																		lblNacionalidad)
																.addPreferredGap(
																		ComponentPlacement.RELATED)
																.addComponent(
																		jLabel6)))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(jComboBox_partido,
										GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addComponent(lblMesa,
										GroupLayout.PREFERRED_SIZE, 19,
										GroupLayout.PREFERRED_SIZE)
								.addGap(6)
								.addComponent(jComboBox_mesa,
										GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addGap(18)
								.addGroup(
										layout.createParallelGroup(
												Alignment.BASELINE)
												.addComponent(
														jButton_seleccionar)
												.addComponent(btnVolver))
								.addContainerGap(199, Short.MAX_VALUE)));
		getContentPane().setLayout(layout);

		pack();
	}// </editor-fold>//GEN-END:initComponents

	public void setApellido(String txt) {
		this.lblApellido.setText("Apellido: " + txt);
	}

	public void setAvatar(JLabel jLabel_avatar) {
		this.jLabel_avatar = jLabel_avatar;
	}

	public void setNacionalidad(String txt) {
		this.jLabel6.setText("Nacionalidad: " + txt);
	}

	public void setNombre(String txt) {
		this.lblNombre.setText("Nombre: " + txt);
	}

	public void setSexo(String txt) {
		this.lblNacionalidad.setText("Sexo: " + txt);
	}

	public String getDni() {
		return jTextField_dni.getText();
	}

	public Partido getPartido() {
		return (Partido) this.jComboBox_partido.getSelectedItem();
	}

	public Mesa getMesa() {
		return (Mesa) this.jComboBox_mesa.getSelectedItem();
	}

	public void setMesas(List<Mesa> lstmesas) {
		for (Mesa c : lstmesas) {
			this.jComboBox_mesa.addItem(c);
		}
	}

	public void setPartidos(List<Partido> lstmesas) {
		for (Partido c : lstmesas) {
			this.jComboBox_partido.addItem(c);
		}
	}

	public void buscarListener(ActionListener listen) {
		jButton_buscar.addActionListener(listen);
	}

	public void crearListener(ActionListener listen) {
		jButton_seleccionar.addActionListener(listen);
	}

	public void displayErrorMessage(String errorMessage) {
		JOptionPane.showMessageDialog(this, errorMessage, "Error",
				JOptionPane.ERROR_MESSAGE);
	}

	public void displayWarningMessage(String wrngMessage) {
		JOptionPane.showMessageDialog(this, wrngMessage, "Aviso",
				JOptionPane.WARNING_MESSAGE);
	}

	public void displayInfoMessage(String infoMessage) {
		JOptionPane.showMessageDialog(this, infoMessage, "",
				JOptionPane.INFORMATION_MESSAGE);
	}

	// Variables declaration - do not modify//GEN-BEGIN:variables
	private JButton jButton_buscar;
	private JButton jButton_seleccionar;
	private JComboBox jComboBox_partido;
	private JComboBox jComboBox_mesa;
	private JLabel jLabel1;
	private JLabel jLabel2;
	private JLabel lblNombre;
	private JLabel lblApellido;
	private JLabel lblNacionalidad;
	private JLabel jLabel6;
	private JLabel jLabel7;
	private JLabel jLabel_avatar;
	private JSeparator jSeparator1;
	private JTextField jTextField_dni;
	private JButton btnVolver;
}

package view.altas;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

//import java.io.*;
//import java.awt.*;
//import java.awt.event.*;
//import javax.swing.*;

public class AltaPartidoGUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField txtNombre = new JTextField(null, 10);
	private JTextField txtImagen = new JTextField(null, 20);

	private JLabel lblNombre = new JLabel("Nombre");
	private JLabel lblImagen = new JLabel("Imagen");
	private JLabel lblCPrimario = new JLabel("Color Primario");
	private JLabel lblCSecundario = new JLabel("Color Secundario");
	private JLabel lblMuestraCPrimario = new JLabel("     ");
	private JLabel lblMuestraCSecundario = new JLabel("     ");

	private JFileChooser fcImagen = new JFileChooser();

	private JButton btnBuscarImagen = new JButton("Buscar Imagen");
	private JButton btnGuardar = new JButton("Guardar");
	private JButton btnCPrimario = new JButton("Seleccione Color");
	private JButton btnCSecundario = new JButton("Seleccione Color");
	private final JPanel panel = new JPanel();
	private final JLabel lblTitle = new JLabel("Agregar Nuevo Partido");
	private final JPanel panel_1 = new JPanel();
	private final JPanel panel_2 = new JPanel();
	private final JPanel panel_3 = new JPanel();
	private final JPanel panel_4 = new JPanel();
	private final JPanel panel_5 = new JPanel();

	// private JButton btnEnter = new JButton("OK");
	public AltaPartidoGUI() {

		// Sets up the view and adds the components

		JPanel nombrePanel = new JPanel();

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(1000, 700);

		getContentPane().add(nombrePanel, BorderLayout.NORTH);
		lblTitle.setFont(new Font("Tahoma", Font.PLAIN, 21));
		lblTitle.setHorizontalAlignment(SwingConstants.LEFT);

		nombrePanel.add(lblTitle);

		getContentPane().add(panel, BorderLayout.EAST);
		FlowLayout flowLayout = (FlowLayout) panel_1.getLayout();
		flowLayout.setAlignOnBaseline(true);

		panel.add(panel_1);
		panel_1.add(lblNombre);
		panel_1.add(txtNombre);

		panel.add(panel_2);
		panel_2.add(lblImagen);
		panel_2.add(txtImagen);
		panel_2.add(btnBuscarImagen);

		// btnGuardar.addActionListener(new AltaPartidoGUI.OpenListener());
		btnBuscarImagen.addActionListener(new AltaPartidoGUI.OpenListener());

		panel.add(panel_3);
		panel_3.add(lblCPrimario);
		panel_3.add(lblMuestraCPrimario);
		lblMuestraCPrimario.setBackground(Color.WHITE);
		lblMuestraCPrimario.setOpaque(true);
		panel_3.add(btnCPrimario);
		btnCPrimario.addActionListener(new cPrimarioListener());

		panel.add(panel_4);
		panel_4.add(lblCSecundario);
		panel_4.add(lblMuestraCSecundario);
		lblMuestraCSecundario.setBackground(Color.WHITE);
		lblMuestraCSecundario.setOpaque(true);
		panel_4.add(btnCSecundario);
		btnCSecundario.addActionListener(new cSecundarioListener());

		panel.add(panel_5);
		panel_5.add(btnGuardar);
		// End of setting up the components --------

	}

	private class cPrimarioListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Color c = JColorChooser.showDialog(null, "Choose a Color",
					lblMuestraCPrimario.getBackground());
			if (c != null)
				lblMuestraCPrimario.setOpaque(true);
			lblMuestraCPrimario.setBackground(c);
		}
	}

	private class cSecundarioListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Color c = JColorChooser.showDialog(null, "Choose a Color",
					lblMuestraCSecundario.getBackground());
			if (c != null)
				lblMuestraCSecundario.setOpaque(true);
			lblMuestraCSecundario.setBackground(c);
		}
	}

	private class OpenListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			int returnVal = fcImagen.showOpenDialog(AltaPartidoGUI.this);

			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fcImagen.getSelectedFile();
				// this is where a real application would open the file.
				txtImagen.setText(file.getAbsolutePath());
			} else {
				// JOptionPane.showMessageDialog(null,
				// "Open command cancelled by user.");
			}
		}
	}

	public String getNombre() {
		return txtNombre.getText();
	}

	public String getImagen() {
		return txtImagen.getText();
	}

	public String getCPrimario() {
		String rgb = Integer.toHexString(lblMuestraCPrimario.getBackground()
				.getRGB());
		return "#" + rgb.substring(2, rgb.length()).toUpperCase();
	}

	public String getCSecundario() {
		String rgb = Integer.toHexString(lblMuestraCSecundario.getBackground()
				.getRGB());
		return "#" + rgb.substring(2, rgb.length()).toUpperCase();
	}

	public void addGuardarListener(ActionListener listenForOkButton) {

		btnGuardar.addActionListener(listenForOkButton);

	}

	// Open a popup that contains the message passed

	public void displayErrorMessage(String errorMessage) {
		JOptionPane.showMessageDialog(this, errorMessage, "Error",
				JOptionPane.ERROR_MESSAGE);
	}

	public void displayWarningMessage(String wrngMessage) {
		JOptionPane.showMessageDialog(this, wrngMessage, "Aviso",
				JOptionPane.WARNING_MESSAGE);
	}

	public void displayInfoMessage(String infoMessage) {
		JOptionPane.showMessageDialog(this, infoMessage, "",
				JOptionPane.INFORMATION_MESSAGE);
	}

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package view.votacion;

import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.UIManager;

public class AutentificacionDelVotanteGUI extends javax.swing.JFrame {
	private JLabel lblVotacin;
	private JLabel lblPersonasRestantesPara;
	private JLabel lblVotantesRestantes;
	private JTextField textDni;
	private JButton btnBuscarPersona;
	private JSeparator separator;
	private JLabel lblNombreCompleto;
	private JLabel lblEdad;
	private JLabel lblSexo;
	private JLabel lblNacionalidad;
	private JLabel lblFechaDeNacimiento;
	private JLabel lblPaisDeOrigen;
	private JLabel lblDireccion;
	private JLabel lblVotoRealizado;
	private JButton btnIniciarVotacion;
	private JButton btnImprimirElComprobante;
	private JSeparator separator_2;
	private JLabel lblAvatar;

	public AutentificacionDelVotanteGUI() {
		lblVotacin = new JLabel();
		lblPersonasRestantesPara = new JLabel();
		lblVotantesRestantes = new JLabel();
		textDni = new JTextField();
		btnBuscarPersona = new JButton();
		separator = new JSeparator();
		lblNombreCompleto = new JLabel();
		lblEdad = new JLabel();
		lblSexo = new JLabel();
		lblNacionalidad = new JLabel();
		lblFechaDeNacimiento = new JLabel();
		lblPaisDeOrigen = new JLabel();
		lblDireccion = new JLabel();
		lblVotoRealizado = new JLabel();
		btnIniciarVotacion = new JButton();
		btnImprimirElComprobante = new JButton();
		separator_2 = new JSeparator();
		lblAvatar = new JLabel();

		btnIniciarVotacion.setEnabled(false);
		btnImprimirElComprobante.setEnabled(false);
		setTitle("Autentificaci\u00F3n del Votante");
		initComponents();
	}

	public String getDni() {
		return textDni.getText();
	}

	public void setAvatar(Image img) {
		ImageIcon icon = new ImageIcon(img);
		lblAvatar.setIcon(icon);
	}

	public void setNombreC(String nc) {
		lblNombreCompleto.setText("Nombre Completo: " + nc.toUpperCase());
	}

	public void setEdad(String e) {
		lblEdad.setText("Edad: " + e.toUpperCase());
	}

	public void setSexo(String s) {
		lblSexo.setText("Sexo: " + s.toUpperCase());
	}

	public void setNacionalidad(String n) {
		lblNacionalidad.setText("Nacionalidad: " + n.toUpperCase());
	}

	public void setFNacimiento(String fn) {
		lblFechaDeNacimiento
				.setText("Fecha de Nacimiento: " + fn.toUpperCase());
	}

	public void setPaisOrigen(String po) {
		lblPaisDeOrigen.setText("Pais de Origen: " + po.toUpperCase());
	}

	public void setDireccion(String d) {
		lblDireccion.setText("Direccion de Residencia: " + d.toUpperCase());
	}

	public void setVoto(String d) {
		lblVotoRealizado.setText("Voto Realizado: " + d.toUpperCase());
	}

	public void setPersonasRestantes(String votantes) {
		lblVotantesRestantes.setText(votantes);
	}

	public void addBuscarListener(ActionListener listen) {
		btnBuscarPersona.addActionListener(listen);
		pack();
	}

	public void addIniciarListener(ActionListener listen) {
		btnIniciarVotacion.addActionListener(listen);
	}

	public void addImprimirListener(ActionListener listen) {
		btnImprimirElComprobante.addActionListener(listen);
	}

	public void enableImprimir(boolean set) {
		btnImprimirElComprobante.setEnabled(set);
	}

	public void enableIniciar(boolean set) {
		btnIniciarVotacion.setEnabled(set);
	}

	public void displayErrorMessage(String errorMessage) {
		JOptionPane.showMessageDialog(this, errorMessage, "Error",
				JOptionPane.ERROR_MESSAGE);
	}

	public void displayWarningMessage(String wrngMessage) {
		JOptionPane.showMessageDialog(this, wrngMessage, "Aviso",
				JOptionPane.WARNING_MESSAGE);
	}

	public void displayInfoMessage(String infoMessage) {
		JOptionPane.showMessageDialog(this, infoMessage, "",
				JOptionPane.INFORMATION_MESSAGE);
	}

	// <editor-fold defaultstate="collapsed"
	// desc="Generated Code">//GEN-BEGIN:initComponents
	private void initComponents() {

		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

		pack();

		lblVotacin = new JLabel("Votaci\u00F3n");
		lblVotacin.setFont(new Font("Georgia", Font.PLAIN, 14));

		lblPersonasRestantesPara = new JLabel(
				"Personas restantes para votar en esta mesa:");

		lblVotantesRestantes = new JLabel("0");

		JLabel lblIngreseElDni = new JLabel("Ingrese el DNI:");

		separator = new JSeparator();

		textDni = new JTextField();
		textDni.setFont(UIManager.getFont("TextArea.font"));
		lblIngreseElDni.setLabelFor(textDni);
		textDni.setColumns(20);

		btnBuscarPersona = new JButton("Buscar Persona");
		btnBuscarPersona.setFont(UIManager.getFont("Button.font"));

		lblNombreCompleto = new JLabel("Nombre Completo:");

		lblEdad = new JLabel("Edad:");

		lblSexo = new JLabel("Sexo:");

		lblNacionalidad = new JLabel("Nacionalidad:");

		lblFechaDeNacimiento = new JLabel("Fecha de Nacimiento:");

		lblPaisDeOrigen = new JLabel("Pa\u00EDs de Origen:");

		lblDireccion = new JLabel("Direcci\u00F3n:");

		separator_2 = new JSeparator();

		lblVotoRealizado = new JLabel("Voto Realizado:");

		btnIniciarVotacion = new JButton("Iniciar Votacion");
		btnIniciarVotacion.setFont(UIManager.getFont("Button.font"));

		btnImprimirElComprobante = new JButton("Imprimir el Comprobante");
		btnImprimirElComprobante.setFont(UIManager.getFont("Button.font"));

		lblAvatar = new JLabel("New label");
		lblAvatar.setIcon(null);
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout
				.setHorizontalGroup(groupLayout
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								groupLayout
										.createSequentialGroup()
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																groupLayout
																		.createSequentialGroup()
																		.addContainerGap()
																		.addComponent(
																				separator_2,
																				GroupLayout.DEFAULT_SIZE,
																				581,
																				Short.MAX_VALUE))
														.addGroup(
																Alignment.TRAILING,
																groupLayout
																		.createSequentialGroup()
																		.addContainerGap()
																		.addComponent(
																				lblVotoRealizado)
																		.addPreferredGap(
																				ComponentPlacement.RELATED,
																				242,
																				Short.MAX_VALUE)
																		.addComponent(
																				btnIniciarVotacion)
																		.addPreferredGap(
																				ComponentPlacement.RELATED)
																		.addComponent(
																				btnImprimirElComprobante))
														.addGroup(
																groupLayout
																		.createSequentialGroup()
																		.addGap(16)
																		.addGroup(
																				groupLayout
																						.createParallelGroup(
																								Alignment.LEADING)
																						.addGroup(
																								groupLayout
																										.createSequentialGroup()
																										.addComponent(
																												lblIngreseElDni)
																										.addGap(5)
																										.addComponent(
																												textDni,
																												GroupLayout.PREFERRED_SIZE,
																												GroupLayout.DEFAULT_SIZE,
																												GroupLayout.PREFERRED_SIZE)
																										.addPreferredGap(
																												ComponentPlacement.RELATED)
																										.addComponent(
																												btnBuscarPersona))
																						.addGroup(
																								groupLayout
																										.createSequentialGroup()
																										.addComponent(
																												lblVotacin)
																										.addPreferredGap(
																												ComponentPlacement.RELATED,
																												272,
																												Short.MAX_VALUE)
																										.addComponent(
																												lblPersonasRestantesPara)
																										.addPreferredGap(
																												ComponentPlacement.RELATED)
																										.addComponent(
																												lblVotantesRestantes,
																												GroupLayout.PREFERRED_SIZE,
																												18,
																												GroupLayout.PREFERRED_SIZE))
																						.addGroup(
																								groupLayout
																										.createSequentialGroup()
																										.addPreferredGap(
																												ComponentPlacement.RELATED)
																										.addGroup(
																												groupLayout
																														.createParallelGroup(
																																Alignment.LEADING)
																														.addComponent(
																																lblFechaDeNacimiento)
																														.addGroup(
																																groupLayout
																																		.createSequentialGroup()
																																		.addComponent(
																																				lblAvatar,
																																				GroupLayout.PREFERRED_SIZE,
																																				137,
																																				GroupLayout.PREFERRED_SIZE)
																																		.addGap(18)
																																		.addGroup(
																																				groupLayout
																																						.createParallelGroup(
																																								Alignment.LEADING)
																																						.addComponent(
																																								lblNacionalidad)
																																						.addComponent(
																																								lblNombreCompleto)
																																						.addComponent(
																																								lblEdad)
																																						.addComponent(
																																								lblSexo)))
																														.addComponent(
																																lblPaisDeOrigen)
																														.addComponent(
																																lblDireccion)))))
														.addGroup(
																groupLayout
																		.createSequentialGroup()
																		.addContainerGap()
																		.addComponent(
																				separator,
																				GroupLayout.DEFAULT_SIZE,
																				581,
																				Short.MAX_VALUE)))
										.addContainerGap()));
		groupLayout
				.setVerticalGroup(groupLayout
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								groupLayout
										.createSequentialGroup()
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																groupLayout
																		.createSequentialGroup()
																		.addGap(9)
																		.addComponent(
																				lblVotacin))
														.addGroup(
																groupLayout
																		.createSequentialGroup()
																		.addContainerGap()
																		.addGroup(
																				groupLayout
																						.createParallelGroup(
																								Alignment.BASELINE)
																						.addComponent(
																								lblVotantesRestantes)
																						.addComponent(
																								lblPersonasRestantesPara))))
										.addPreferredGap(
												ComponentPlacement.RELATED, 8,
												Short.MAX_VALUE)
										.addComponent(separator,
												GroupLayout.PREFERRED_SIZE,
												GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												ComponentPlacement.RELATED)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																groupLayout
																		.createSequentialGroup()
																		.addGap(3)
																		.addComponent(
																				lblIngreseElDni))
														.addGroup(
																groupLayout
																		.createParallelGroup(
																				Alignment.BASELINE)
																		.addComponent(
																				textDni,
																				GroupLayout.PREFERRED_SIZE,
																				GroupLayout.DEFAULT_SIZE,
																				GroupLayout.PREFERRED_SIZE)
																		.addComponent(
																				btnBuscarPersona)))
										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.TRAILING)
														.addGroup(
																groupLayout
																		.createSequentialGroup()
																		.addComponent(
																				lblNombreCompleto)
																		.addPreferredGap(
																				ComponentPlacement.RELATED)
																		.addComponent(
																				lblEdad)
																		.addPreferredGap(
																				ComponentPlacement.RELATED)
																		.addComponent(
																				lblSexo)
																		.addPreferredGap(
																				ComponentPlacement.RELATED)
																		.addComponent(
																				lblNacionalidad))
														.addComponent(
																lblAvatar,
																GroupLayout.PREFERRED_SIZE,
																113,
																GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(
												ComponentPlacement.RELATED)
										.addComponent(lblFechaDeNacimiento)
										.addPreferredGap(
												ComponentPlacement.UNRELATED)
										.addComponent(lblPaisDeOrigen)
										.addPreferredGap(
												ComponentPlacement.UNRELATED)
										.addComponent(lblDireccion)
										.addGap(18)
										.addComponent(separator_2,
												GroupLayout.PREFERRED_SIZE, 4,
												GroupLayout.PREFERRED_SIZE)
										.addGap(9)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																btnImprimirElComprobante)
														.addComponent(
																btnIniciarVotacion)
														.addComponent(
																lblVotoRealizado))
										.addContainerGap()));
		getContentPane().setLayout(groupLayout);
		pack();
	}// </editor-fold>//GEN-END:initComponents
}

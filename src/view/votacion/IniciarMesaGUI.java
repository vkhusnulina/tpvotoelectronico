/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package view.votacion;

import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.UIManager;
import javax.swing.WindowConstants;

public class IniciarMesaGUI extends JFrame {
	private JLabel lblAutoridadDeMesa;
	private JLabel lblIngreseDni;
	private JTextField textDni;
	private JButton btnBuscar;
	private JSeparator separator;
	private JLabel lblMesaNro;
	private JLabel lblInstitucion;
	private JPanel panelAgentes;
	private JButton btnIniciar;
	private JTextArea txtrIntegrantes;

	public IniciarMesaGUI() {
		lblAutoridadDeMesa = new JLabel();
		lblIngreseDni = new JLabel();
		textDni = new JTextField();
		btnBuscar = new JButton();
		separator = new JSeparator();
		lblMesaNro = new JLabel();
		lblInstitucion = new JLabel();
		panelAgentes = new JPanel();
		btnIniciar = new JButton();
		txtrIntegrantes = new JTextArea();
		setTitle("Iniciar Votaci\u00F3n");
		btnIniciar.setEnabled(false);
		initComponents();
	}

	public String getDni() {
		return textDni.getText();
	}

	public void addBuscarListener(ActionListener listen) {
		btnBuscar.addActionListener(listen);
	}

	public void addIniciarListener(ActionListener listen) {
		btnIniciar.addActionListener(listen);
	}

	public void setMesa(int mesa) {
		lblMesaNro.setText("Mesa Nro." + mesa);
	}

	public void setInstitucion(String inst) {
		lblInstitucion.setText("Instituci\u00F3n " + inst.toUpperCase());
	}

	public void setIntegrantes(String integrantes) {
		txtrIntegrantes.setText(integrantes);
		pack();
	}

	public void enableIniciar(boolean set) {
		btnIniciar.setEnabled(set);
	}

	public void displayErrorMessage(String errorMessage) {
		JOptionPane.showMessageDialog(this, errorMessage, "Error",
				JOptionPane.ERROR_MESSAGE);
	}

	public void displayWarningMessage(String wrngMessage) {
		JOptionPane.showMessageDialog(this, wrngMessage, "Aviso",
				JOptionPane.WARNING_MESSAGE);
	}

	public void displayInfoMessage(String infoMessage) {
		JOptionPane.showMessageDialog(this, infoMessage, "",
				JOptionPane.INFORMATION_MESSAGE);
	}

	@SuppressWarnings("unchecked")
	// <editor-fold defaultstate="collapsed"
	// desc="Generated Code">//GEN-BEGIN:initComponents
	private void initComponents() {

		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		pack();

		lblAutoridadDeMesa = new JLabel("Autoridad de Mesa");
		lblAutoridadDeMesa.setFont(new Font("Georgia", Font.PLAIN, 17));

		lblIngreseDni = new JLabel("Ingrese D.N.I.:");
		lblIngreseDni.setFont(new Font("Tahoma", Font.PLAIN, 13));

		textDni = new JTextField();
		textDni.setFont(UIManager.getFont("TextArea.font"));
		lblIngreseDni.setLabelFor(textDni);
		textDni.setColumns(20);

		btnBuscar = new JButton("Buscar");
		btnBuscar.setFont(UIManager.getFont("Button.font"));

		separator = new JSeparator();

		lblMesaNro = new JLabel("Mesa Nro.");

		lblInstitucion = new JLabel("Institucion");

		panelAgentes = new JPanel();

		btnIniciar = new JButton("Iniciar");
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout
				.setHorizontalGroup(groupLayout
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								groupLayout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																groupLayout
																		.createParallelGroup(
																				Alignment.LEADING)
																		.addGroup(
																				groupLayout
																						.createSequentialGroup()
																						.addComponent(
																								separator,
																								GroupLayout.DEFAULT_SIZE,
																								588,
																								Short.MAX_VALUE)
																						.addContainerGap())
																		.addComponent(
																				lblAutoridadDeMesa)
																		.addGroup(
																				groupLayout
																						.createSequentialGroup()
																						.addComponent(
																								lblIngreseDni)
																						.addGap(5)
																						.addComponent(
																								textDni,
																								GroupLayout.PREFERRED_SIZE,
																								GroupLayout.DEFAULT_SIZE,
																								GroupLayout.PREFERRED_SIZE)
																						.addGap(5)
																						.addComponent(
																								btnBuscar)
																						.addContainerGap()))
														.addGroup(
																groupLayout
																		.createSequentialGroup()
																		.addGap(10)
																		.addGroup(
																				groupLayout
																						.createParallelGroup(
																								Alignment.LEADING)
																						.addGroup(
																								Alignment.TRAILING,
																								groupLayout
																										.createSequentialGroup()
																										.addComponent(
																												panelAgentes,
																												GroupLayout.DEFAULT_SIZE,
																												GroupLayout.DEFAULT_SIZE,
																												Short.MAX_VALUE)
																										.addPreferredGap(
																												ComponentPlacement.UNRELATED)
																										.addComponent(
																												btnIniciar)
																										.addContainerGap())
																						.addGroup(
																								groupLayout
																										.createSequentialGroup()
																										.addGroup(
																												groupLayout
																														.createParallelGroup(
																																Alignment.LEADING)
																														.addComponent(
																																lblInstitucion)
																														.addComponent(
																																lblMesaNro))
																										.addGap(538)))))));
		groupLayout
				.setVerticalGroup(groupLayout
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								groupLayout
										.createSequentialGroup()
										.addGap(9)
										.addComponent(lblAutoridadDeMesa)
										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																groupLayout
																		.createSequentialGroup()
																		.addGap(4)
																		.addComponent(
																				lblIngreseDni))
														.addGroup(
																groupLayout
																		.createSequentialGroup()
																		.addGap(1)
																		.addComponent(
																				textDni,
																				GroupLayout.PREFERRED_SIZE,
																				GroupLayout.DEFAULT_SIZE,
																				GroupLayout.PREFERRED_SIZE))
														.addComponent(btnBuscar))
										.addPreferredGap(
												ComponentPlacement.UNRELATED)
										.addComponent(separator,
												GroupLayout.PREFERRED_SIZE, 4,
												GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												ComponentPlacement.UNRELATED)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.TRAILING)
														.addGroup(
																groupLayout
																		.createSequentialGroup()
																		.addComponent(
																				lblMesaNro)
																		.addPreferredGap(
																				ComponentPlacement.UNRELATED)
																		.addComponent(
																				lblInstitucion)
																		.addPreferredGap(
																				ComponentPlacement.RELATED)
																		.addComponent(
																				panelAgentes,
																				GroupLayout.PREFERRED_SIZE,
																				175,
																				GroupLayout.PREFERRED_SIZE))
														.addComponent(
																btnIniciar))
										.addContainerGap()));

		txtrIntegrantes.setBackground(UIManager.getColor("Button.background"));
		txtrIntegrantes.setEnabled(false);
		txtrIntegrantes.setEditable(false);
		panelAgentes.add(txtrIntegrantes);
		getContentPane().setLayout(groupLayout);
		pack();
	}// </editor-fold>//GEN-END:initComponents

}
